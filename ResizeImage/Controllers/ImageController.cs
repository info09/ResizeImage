﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ResizeImage.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageController : ControllerBase
    {
        [HttpGet]
        public IActionResult UpLoad_Image()
        {
            string srcImage = @"C:\Users\traqu\Desktop\1234.jpg";
            //string srcImage = @"http://media.danang24h.vn/2018/1/12/15/1-1515740435.jpg";
            string resizeImage = @"C:\Users\traqu\Desktop\resize.jpg";
            int newsize = 300;
            
            Image_Resize(srcImage, resizeImage, newsize);
            return Ok();
        }
        private void Image_Resize(string input_Image, string output_Image,int newsize)
        {
            const long quality = 50L;
            using (var image = new Bitmap(System.Drawing.Image.FromFile(input_Image)))
            {
                var resize = new Bitmap(newsize, newsize);

                using (var graphic = Graphics.FromImage(resize))
                {
                    graphic.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighSpeed;
                    graphic.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    graphic.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
                    graphic.DrawImage(image, 0, 0, newsize, newsize);

                    using (var output = System.IO.File.Open(output_Image, System.IO.FileMode.Create))
                    {
                        var qualityParamId = Encoder.Quality;
                        var encoderParameters = new EncoderParameters(1);
                        encoderParameters.Param[0] = new EncoderParameter(qualityParamId, quality);

                        var codec = ImageCodecInfo.GetImageDecoders().FirstOrDefault(i => i.FormatID == ImageFormat.Jpeg.Guid);
                        resize.Save(output, codec, encoderParameters);
                    }
                }
            }
        }
    }
}